﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MonsterDuos
{
    public class ButtonListEntry : MonoBehaviour
    {
        public Button Button;
        [SerializeField] TextMeshProUGUI textField;
        public string Text { get { return textField.text; } set { textField.text = value; } }
    }
}
