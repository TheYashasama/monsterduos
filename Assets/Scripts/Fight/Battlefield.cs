﻿using System.Collections.Generic;
using UnityEngine;

namespace MonsterDuos
{
    public class Battlefield : MonoBehaviour
    {
        [SerializeField] List<Monster> playerMonsters;
        [SerializeField] List<Monster> opponentMonsters;

        private void Start()
        {
            ListenForRefresh(playerMonsters);
            ListenForRefresh(opponentMonsters);
        }

        void ListenForRefresh(List<Monster> monsters)
        {
            foreach (var current in monsters)
            {
                current.DeathEvent += RemoveMonster;
            }
        }

        private void RemoveMonster(Monster monster)
        {
            if (playerMonsters.Contains(monster)) playerMonsters.Remove(monster);
            else if (opponentMonsters.Contains(monster)) opponentMonsters.Remove(monster);
        }

        public List<Monster> Monsters
        {
            get { return playerMonsters; }
        }
        public List<Monster> Opponents
        {
            get { return opponentMonsters; }
        }
        public int NumberOfMonsters
        {
            get { return playerMonsters.Count; }
        }
        public int NumberOfOpponents
        {
            get { return opponentMonsters.Count; }
        }


    }
}