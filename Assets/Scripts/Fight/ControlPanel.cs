﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MonsterDuos
{
    public class ControlPanel : MonoBehaviour, IPersistable
    {
        [SerializeField] GameDataStorage gameDataStorage;
        [SerializeField] RectTransform monsterDisplayContainer;
        [SerializeField] GameObject monsterDisplayPrefab;

        [SerializeField] RectTransform moveButtonContainer;
        [SerializeField] RectTransform targets;
        [SerializeField] RectTransform opponentButtonContainer;
        [SerializeField] TextMeshProUGUI selectedMove;
        Button[] moveButtons;
        Button[] opponentButtons;

        List<MonsterDisplay> monsterDisplays = new List<MonsterDisplay>();
        LoopManager loopManager;
        Battlefield battlefield;

        int monsterIndex;
        int chosenMove;

        Monster Monster { get { return battlefield.Monsters[monsterIndex]; } }

        MoveData[] CurrentMoves
        {
            get
            {
                if (!Monster || Monster.Moves == null)
                {
                    throw new Exception("No MonsterData or No Moves");
                }
                return Monster.Moves;
            }
        }

        private void Awake()
        {
            loopManager = FindObjectOfType<LoopManager>();
            battlefield = FindObjectOfType<Battlefield>();
            moveButtons = moveButtonContainer.GetComponentsInChildren<Button>();
            if (moveButtons.Length != Utilities.MaximumMoves) Debug.LogError($"There need to be {Utilities.MaximumMoves} move buttons");
            opponentButtons = opponentButtonContainer.GetComponentsInChildren<Button>();

            GenerateMonsterDisplays();
            SetTargets();
        }

        private void GenerateMonsterDisplays()
        {
            RemoveMonsterDisplays();
            monsterDisplays.Clear();
            foreach (var monster in battlefield.Monsters)
            {
                MonsterDisplay display = Instantiate(monsterDisplayPrefab, monsterDisplayContainer).GetComponent<MonsterDisplay>();
                display.SetMonster(monster);
                monsterDisplays.Add(display);
            }
        }

        private void RemoveMonsterDisplays()
        {
            for (int i = monsterDisplayContainer.childCount - 1; i >= 0; i--)
            {
                Destroy(monsterDisplayContainer.GetChild(i).gameObject);
            }
        }

        void SetTargets()
        {
            for (int i = 0; i < opponentButtons.Length; i++)
            {
                bool opponentExists = i < battlefield.NumberOfOpponents;
                opponentButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = opponentExists ? battlefield.Opponents[i].ToString() : "None";
                opponentButtons[i].interactable = opponentExists;
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                SelectMove(0);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                SelectMove(1);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                SelectMove(2);
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                SelectMove(3);
            }


            if (Input.GetKeyDown(KeyCode.S))
            {
                gameDataStorage.Save(this);
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                gameDataStorage.Load(this);
            }
        }

        public void StartChoice()
        {
            monsterIndex = 0;
            RefreshUI();
        }

        void NextChoice()
        {
            if (monsterIndex < battlefield.NumberOfMonsters - 1)
            {
                monsterIndex++;
            }
            RefreshUI();
        }

        void RefreshUI()
        {
            UpdateMoves();
            SetTargets();
            HideTargets();
            HighlightCurrentMonster();
        }

        void HighlightCurrentMonster()
        {
            for (int i = 0; i < monsterDisplays.Count; i++)
            {
                bool highlight = monsterIndex == i;
                monsterDisplays[i].SetHighlight(highlight);
            }
        }

        void DeactivateHighlight()
        {
            foreach (var display in monsterDisplays)
            {
                display.SetHighlight(false);
            }
        }

        public void EndChoice()
        {
            DisableMoves();
            DeactivateHighlight();
        }

        void DisableMoves()
        {
            foreach (var item in moveButtons)
            {
                item.interactable = false;
            }
        }

        void UpdateMoves()
        {
            for (int i = 0; i < moveButtons.Length; i++)
            {
                moveButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = MoveName(i);
                moveButtons[i].interactable = i < CurrentMoves.Length && CurrentMoves[i] != null;
            }
        }

        string MoveName(int index)
        {
            return $"{Monster.GetMoveName(index)}\n[{index + 1}]";
        }

        public void SelectMove(int index)
        {
            if (!Monster.CheckForMove(index)) return;

            chosenMove = index;
            selectedMove.text = $"Use {Monster.Moves[index].name} on...";
            ShowTargets();
        }

        void ShowTargets()
        {
            targets.gameObject.SetActive(true);
        }

        void HideTargets()
        {
            targets.gameObject.SetActive(false);
        }

        public void SelectTarget(int index)
        {
            loopManager.AddPlayerChoice(new LoopManager.MoveChoice(Monster, chosenMove, battlefield.Opponents[index]));
            NextChoice();
        }

        public void Save(GameDataWriter writer)
        {
            Debug.LogWarning("SAVING");
            foreach (var item in battlefield.Monsters)
            {
                item.Save(writer);
            }
        }

        public void Load(GameDataReader reader)
        {
            Debug.LogWarning("LOADING");
            foreach (var item in battlefield.Monsters)
            {
                item.Load(reader);
            }
            GenerateMonsterDisplays();
            RefreshUI();
        }
    }
}
