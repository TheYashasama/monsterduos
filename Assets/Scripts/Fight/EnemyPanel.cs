﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MonsterDuos
{
    public class EnemyPanel : MonoBehaviour
    {
        [SerializeField] RectTransform monsterDisplayContainer;
        [SerializeField] GameObject monsterDisplayPrefab;

        List<MonsterDisplay> monsterDisplays = new List<MonsterDisplay>();
        Battlefield battlefield;
        LoopManager loopManager;

        private void Awake()
        {
            battlefield = FindObjectOfType<Battlefield>();
            loopManager = FindObjectOfType<LoopManager>();
        }

        void Start()
        {
            foreach (var monster in battlefield.Opponents)
            {
                MonsterDisplay display = Instantiate(monsterDisplayPrefab, monsterDisplayContainer).GetComponent<MonsterDisplay>();
                display.SetMonster(monster);
                monsterDisplays.Add(display);
            }
        }

        public void StartChoice()
        {
            foreach (var monster in battlefield.Opponents)
            {
                Monster opponent = GetRandomOpponent();
                loopManager.SetEnemyChoice(new LoopManager.MoveChoice(monster, GetRandomMove(monster), opponent));
            }
        }

        int GetRandomMove(Monster monster)
        {
            return UnityEngine.Random.Range(0, monster.NumberOfMoves);
        }

        Monster GetRandomOpponent()
        {
            return battlefield.Monsters[UnityEngine.Random.Range(0, battlefield.NumberOfMonsters)];
        }
    }
}
