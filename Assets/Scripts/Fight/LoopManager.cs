﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace MonsterDuos
{
    public class LoopManager : MonoBehaviour
    {
        public struct MoveChoice
        {
            public Monster monster;
            public Monster target;
            public int move;

            public MoveChoice(Monster monster, int move, Monster target)
            {
                this.monster = monster;
                this.move = move;
                this.target = target;
            }

            public bool IsValid
            {
                get
                {
                    return monster.IsAlive && target.IsAlive;
                }
            }
        }

        [SerializeField] ControlPanel playerControl;
        [SerializeField] EnemyPanel enemyControl;
        [SerializeField] TextMeshProUGUI stateText;
        [SerializeField] Battlefield battlefield;
        [SerializeField] MoveAnimator moveAnimator;

        List<MoveChoice> choices = new List<MoveChoice>();
        bool playerFinished;
        bool opponentFinished;

        void Start()
        {
            StartCoroutine(PlayerPlanning());
        }

        IEnumerator PlayerPlanning()
        {
            playerFinished = false;
            stateText.text = "Choose your action";

            playerControl.StartChoice();
            while (!playerFinished)
            {
                yield return new WaitForEndOfFrame();
            }
            playerControl.EndChoice();

            StartCoroutine(EnemyPlanning());
        }

        public void AddPlayerChoice(MoveChoice moveChoice)
        {
            choices.Add(moveChoice);
            playerFinished = choices.Count == battlefield.NumberOfMonsters;
        }

        IEnumerator EnemyPlanning()
        {
            opponentFinished = false;
            stateText.text = "Opponent";

            enemyControl.StartChoice();
            while (!opponentFinished)
            {
                yield return new WaitForSecondsRealtime(0.25f);
            }

            StartCoroutine(Resolve());
        }

        public void SetEnemyChoice(MoveChoice moveChoiced)
        {
            choices.Add(moveChoiced);
            opponentFinished = choices.Count == battlefield.NumberOfMonsters + battlefield.NumberOfOpponents;
        }

        IEnumerator Resolve()
        {
            stateText.text = "Resolve!";

            choices.Sort(SortMoveChoice);

            foreach (var choice in choices)
            {
                if (choice.IsValid)
                {
                    float delay = moveAnimator.Animate($"{choice.monster.Name} used {choice.monster.GetMoveName(choice.move)}");
                    yield return new WaitForSecondsRealtime(delay + 0.15f);
                    choice.monster.AttackMonster(choice.move, choice.target);
                }
            }

            if (battlefield.NumberOfOpponents <= 0)
            {
                Win();
            }
            else if (battlefield.NumberOfMonsters <= 0)
            {
                Lose();
            }
            else
            {
                choices.Clear();
                StartCoroutine(PlayerPlanning());
            }
        }

        void Lose()
        {
            stateText.text = "You Lost!";
        }

        void Win()
        {
            stateText.text = "You Won!";
        }

        private int SortMoveChoice(MoveChoice x, MoveChoice y)
        {
            return y.monster.Speed.CompareTo(x.monster.Speed);
        }
    }
}