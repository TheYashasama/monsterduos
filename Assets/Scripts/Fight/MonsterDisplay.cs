﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MonsterDuos
{
    public class MonsterDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] Image background;
        [SerializeField] Color backgroundColor;
        [SerializeField] Color highlightedColor;

        [SerializeField] TextMeshProUGUI levelText;
        [SerializeField] TextMeshProUGUI nameText;
        [SerializeField] Image hpImage;
        [SerializeField] TextMeshProUGUI hpText;
        [SerializeField] bool hideNumbers;
        [SerializeField] StatsDisplay toolTip;
        [SerializeField] float tooltipHideDelay;
        [SerializeField] bool disableTooltip;

        Monster currentMonster;
        private float showTooltipTime;
        private Coroutine hideCoroutine;

        private void Start()
        {
            hpText.gameObject.SetActive(!hideNumbers);
            SetHighlight(false);
        }

        private void OnDestroy()
        {
            RemoveEvents();
        }

        public void SetMonster(Monster monster)
        {
            RemoveEvents();
            currentMonster = monster;
            currentMonster.RefreshEvent += UpdateMonster;
            currentMonster.DeathEvent += RemoveMonster;

            UpdateMonster(monster);
        }

        private void RemoveEvents()
        {
            if (currentMonster)
            {
                currentMonster.RefreshEvent -= UpdateMonster;
                currentMonster.DeathEvent -= RemoveMonster;
            }
        }

        void RemoveMonster(Monster monster)
        {
            currentMonster = null;
            UpdateMonster(monster);
        }

        public void UpdateMonster(Monster monster)
        {
            if (currentMonster == null)
            {
                gameObject.SetActive(false);
                return;
            }
            gameObject.SetActive(true);

            levelText.text = currentMonster.Level;
            nameText.text = currentMonster.Name;
            hpImage.fillAmount = currentMonster.CurrentHPPercentage;
            if (!hideNumbers) hpText.text = currentMonster.HP;

            toolTip.Initialize(monster);
        }

        public void SetHighlight(bool state)
        {
            background.color = state ? highlightedColor : backgroundColor;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (disableTooltip) return;

            toolTip.Show();
            if (hideCoroutine != null) StopCoroutine(hideCoroutine);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (hideCoroutine != null) StopCoroutine(hideCoroutine);
            hideCoroutine = StartCoroutine(HideTooltip());
        }

        IEnumerator HideTooltip()
        {
            float delay = Time.time + tooltipHideDelay;
            while (Time.time < delay)
            {
                yield return new WaitForEndOfFrame();
            }

            toolTip.Hide();
        }
    }
}
