﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace MonsterDuos
{

    public class MoveAnimator : MonoBehaviour
    {
        [SerializeField] float animationDuration;
        [SerializeField] Animator animator;
        [SerializeField] TextMeshProUGUI text;

        void Start()
        {
            animator = GetComponent<Animator>();
            text = GetComponentInChildren<TextMeshProUGUI>();
        }

        public float Animate(string text)
        {
            gameObject.SetActive(true);
            this.text.text = text;
            StartCoroutine(DoAnimation());
            return animationDuration;
        }

        IEnumerator DoAnimation()
        {
            animator.SetTrigger("DoAnimation");
            yield return new WaitForSeconds(animationDuration);
            Hide();
        }

        void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
