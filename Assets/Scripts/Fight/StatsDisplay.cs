﻿using TMPro;
using UnityEngine;

namespace MonsterDuos
{
    public class StatsDisplay : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI hpText;
        [SerializeField] TextMeshProUGUI attackText;
        [SerializeField] TextMeshProUGUI defenseText;
        [SerializeField] TextMeshProUGUI speedText;

        public void Initialize(Monster monster)
        {
            hpText.text = $"HP: {monster.HP}";
            attackText.text = $"Attack: {monster.Attack}";
            defenseText.text = $"Defense: {monster.Defense}";
            speedText.text = $"Speed: {monster.Speed}";
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
