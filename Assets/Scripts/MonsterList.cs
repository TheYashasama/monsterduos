﻿using System.Collections.Generic;
using UnityEngine;

namespace MonsterDuos
{
    public class MonsterList : MonoBehaviour
    {
        [SerializeField] List<MonsterData> monsters;
        [SerializeField] GameObject listEntryPrefab;
        [SerializeField] RectTransform entryContainer;
        [SerializeField] MonsterFactory factory;
        [SerializeField] Team team;
        [SerializeField] TeamDisplay teamDisplay;

        void Start()
        {
            foreach (var monster in monsters)
            {
                ButtonListEntry entry = Instantiate(listEntryPrefab, entryContainer).GetComponent<ButtonListEntry>();
                MonsterData md = monster;
                entry.Text = monster.name;
                entry.Button.onClick.AddListener(() => AddToTeam(md));
            }
        }

        void AddToTeam(MonsterData monsterData)
        {
            Monster monster = factory.InstantiateMonster(transform);
            monsterData.InitializeMonster(monster, 5);
            team.AddMonster(monster);
            teamDisplay.UpdateDisplay();
        }
    }
}