﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MonsterDuos {
    [CreateAssetMenu(menuName ="MonsterDuos/Element")]
	public class Element : ScriptableObject {

        public override string ToString()
        {
            return "ELement: " + name;
        }

        [SerializeField] List<Element> Weaknesses = new List<Element>();
        [SerializeField] List<Element> Resistances = new List<Element>();

        public bool IsWeakAgainst(Element element)
        {
            return Weaknesses.Contains(element);
        }

        public bool IsResistantAgainst(Element element)
        {
            return Resistances.Contains(element);
        }

        public float GetMoveModifier(Element targetElement)
        {
            return targetElement.IsResistantAgainst(this) ? Utilities.ResitanceModifer : (targetElement.IsWeakAgainst(this) ? Utilities.WeaknessModifier : 1f);
        }

        public float GetMoveModifier(Monster targetMonster)
        {
            return GetMoveModifier(targetMonster.Element);
        }
    }
}