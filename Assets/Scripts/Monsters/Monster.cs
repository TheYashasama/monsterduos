﻿using System.Collections;
using UnityEngine;

namespace MonsterDuos
{
    public class Monster : MonoBehaviour, IPersistable
    {
        public delegate void MonsterEvent(Monster monster);
        public MonsterEvent RefreshEvent;
        public MonsterEvent DeathEvent;

        [SerializeField] int initialLevel;
        [SerializeField] MonsterData data;


        MonsterStats stats;
        public string HP
        {
            get
            {
                return $"{stats.HPCurrent}/{stats.HP}";
            }
        }

        public float CurrentHPPercentage
        {
            get
            {
                return (float)stats.HPCurrent / stats.HP;
            }
        }

        public int Speed { get { return stats.Speed; } }

        public string Name { get { return data.name; } }
        public Element Element { get { return data.Element; } }

        public MoveData[] Moves { get { return data.Moves; } }
        public int NumberOfMoves
        {
            get
            {
                for (int i = 0; i < Moves.Length; i++)
                {
                    if (Moves[i] == null) return i;
                }
                return Utilities.MaximumMoves;
            }
        }

        public bool IsAlive { get { return stats.HPCurrent > 0; } }

        public int Attack { get { return stats.Attack; } }
        public int Defense { get { return stats.Defense; } }

        public string Level { get { return stats.Level.ToString(); } }

        void Awake()
        {
            data.InitializeMonster(this, initialLevel);
        }

        public void Initialize(MonsterData monsterData, MonsterStats stats, Material material)
        {
            data = monsterData;
            GetComponentInChildren<Renderer>().material = material;

            this.stats = stats;
            gameObject.name = Name;

        }

        public void LevelUp()
        {
            data.LevelUpMonster(this);
        }

        internal void GainStats(int level, int hpGain, int attackGain, int defenseGain, int speedGain)
        {
            stats.Level += level;
            stats.HP += hpGain;
            stats.HPCurrent += hpGain;
            stats.Attack += attackGain;
            stats.Defense += defenseGain;
            stats.Speed += speedGain;
        }

        public bool CheckForMove(int moveIndex)
        {
            return moveIndex < Moves.Length && Moves[moveIndex] != null;
        }

        public string GetMoveName(int moveIndex)
        {

            return CheckForMove(moveIndex) ? Moves[moveIndex].name : "None";
        }

        public void TakeDamage(int damage)
        {
            stats.HPCurrent -= damage;
            RefreshEvent?.Invoke(this);
            if (!IsAlive)
            {
                StartCoroutine(Die());
            }
        }

        public void AttackMonster(int moveIndex, Monster other)
        {
            if (!CheckForMove(moveIndex)) return;

            MoveData move = Moves[moveIndex];
            if (UnityEngine.Random.Range(1, 101) > move.Accuracy)
            {
                Debug.Log($"{Name} used {move} on {other.Name} but missed.");
            }
            else
            {
                Debug.Log($"{Name} used {move} on {other.Name}. Effectiveness: {move.GetModifier(other)} Bonus: {move.GetBonus(this)}");
                other.TakeDamage(Utilities.CalculateDamage(this, other, move));
            }
        }

        IEnumerator Die()
        {
            DeathEvent?.Invoke(this);
            Debug.Log(this + " died.");
            yield return new WaitForSecondsRealtime(Utilities.DeathTimer);
            Destroy(gameObject);
        }

        public override string ToString()
        {
            return data.ToString();
        }

        MonsterFactory factory;
        public MonsterFactory Factory
        {
            private get
            {
                if(!factory)
                {
                    factory = FindObjectOfType<MonsterFactory>();
                }
                return factory;
            }
            set
            {
                factory = value;
            }
        }

        public void Save(GameDataWriter writer)
        {
            writer.Write(data.ID);
            writer.Write(stats);
        }

        public void Load(GameDataReader reader)
        {
            int id = reader.ReadInt();
            MonsterStats stats = reader.ReadStats();
            Factory.GetDataForID(id).InitializeMonster(this, stats);
        }
    }
}