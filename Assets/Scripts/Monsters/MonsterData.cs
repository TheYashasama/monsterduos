﻿using UnityEngine;


namespace MonsterDuos
{
    [CreateAssetMenu(menuName = "MonsterDuos/Monster Data")]
    public class MonsterData : ScriptableObject
    {
        [SerializeField] int id;
        public int ID { get { return id; } }
        [SerializeField] Element element;
        public Element Element { get { return element; } }

        [SerializeField] [Range(2, 5)] int hpMaxPerLevel;
        [SerializeField] int hpStarting;
        [SerializeField] [Range(2, 5)] int attackMaxPerLevel;
        [SerializeField] int attackStarting;
        [SerializeField] [Range(2, 5)] int defenseMaxPerLevel;
        [SerializeField] int defenseStarting;
        [SerializeField] [Range(2, 5)] int speedMaxPerLevel;
        [SerializeField] int speedStarting;


        public void InitializeMonster(Monster monster, int level)
        {
            level = Mathf.Clamp(level, 1, Utilities.MaximumMonsterlevel);

            int hp = hpStarting;
            int attack = attackStarting;
            int defense = defenseStarting;
            int speed = speedStarting;
            for (int i = 0; i < level; i++)
            {
                hp += Random.Range(0, hpMaxPerLevel + 1);
                attack += Random.Range(0, attackMaxPerLevel + 1);
                defense += Random.Range(0, defenseMaxPerLevel + 1);
                speed += Random.Range(0, speedMaxPerLevel + 1);
            }

            monster.Initialize(this, new MonsterStats(level, hp, attack, defense, speed, 0), material);
        }

        public void InitializeMonster(Monster monster, MonsterStats stats)
        { 
            monster.Initialize(this, stats, material);
        }

        public void LevelUpMonster(Monster monster)
        {
            int hpGain = Random.Range(0, hpMaxPerLevel + 1);
            int attackGain = Random.Range(0, attackMaxPerLevel + 1);
            int defenseGain = Random.Range(0, defenseMaxPerLevel + 1);
            int speedGain = Random.Range(0, speedMaxPerLevel + 1);

            monster.GainStats(1, hpGain, attackGain, defenseGain, speedGain);
        }

        [SerializeField] MoveData[] moves = new MoveData[4];

        public MoveData[] Moves
        {
            get
            {
                if (moves.Length != Utilities.MaximumMoves)
                {
                    Debug.LogError($"{name}: The Number of move slots must always be {Utilities.MaximumMoves}");
                }
                return moves;
            }
        }

        [SerializeField] Material material;

        public override string ToString()
        {
            return name;
        }
    }
}
