﻿using System.Collections.Generic;
using UnityEngine;

namespace MonsterDuos
{
    public class MonsterFactory : MonoBehaviour
    {
        [SerializeField] List<MonsterData> monsterDatas;
        [SerializeField] GameObject monsterPrefab;
        Dictionary<int, MonsterData> monsterDataDictionary;

        private void Awake()
        {
            monsterDataDictionary = new Dictionary<int, MonsterData>();
            foreach (var data in monsterDatas)
            {
                monsterDataDictionary.Add(data.ID, data);
            }
        }

        public MonsterData GetDataForID(int id)
        {
            return monsterDataDictionary[id];
        }

        public Monster InstantiateMonster(Transform parent)
        {
            Monster monster = Instantiate(monsterPrefab, parent).GetComponent<Monster>();
            monster.Factory = this;
            return monster;
        } 
    }
}
