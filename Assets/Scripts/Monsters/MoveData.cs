﻿using UnityEngine;
namespace MonsterDuos
{
    [CreateAssetMenu(menuName = "MonsterDuos/Attack Data")]
    public class MoveData : ScriptableObject
    {
        [SerializeField] Element element;
        [SerializeField] int power = 40;
        [SerializeField] int accuracy = 100;

        public int Power
        {
            get
            {
                return power;
            }
        }

        public int Accuracy
        {
            get
            {
                return accuracy;
            }
        }

        public Element Element
        {
            get
            {
                return element;
            }
        }

        public float GetModifier(Monster target)
        {
            return element.GetMoveModifier(target);
        }

        public float GetBonus(Monster usingMonster)
        {
            return usingMonster.Element == Element ? Utilities.ElementBonus : 1f;
        }

        public override string ToString()
        {
            return $"Move: {name}  ({power} - {accuracy})";
        }
    }
}
