﻿using System.Collections.Generic;
using UnityEngine;

namespace MonsterDuos
{
    public class Team : MonoBehaviour, IPersistable
    {
        [SerializeField] string teamID;
        [SerializeField] List<Monster> monsters = new List<Monster>();

        public List<Monster> Monsters { get { return monsters; } }

        public void Load(GameDataReader reader)
        {
            MonsterFactory factory = FindObjectOfType<MonsterFactory>();
            monsters.Clear();
            teamID = reader.ReadString();
            int count = reader.ReadInt();
            for (int i = 0; i < count; i++)
            {
                Monster monster = factory.InstantiateMonster(transform);
                monster.Load(reader);
                monsters.Add(monster);
            }
        }

        public void Save(GameDataWriter writer)
        {
            writer.Write(teamID);
            writer.Write(monsters.Count);
            foreach (var monster in monsters)
            {
                monster.Save(writer);
            }
        }

        public void AddMonster(Monster monster)
        {
            if (monsters.Count >= Utilities.TeamSize || monsters.Contains(monster)) return;

            monsters.Add(monster);
            monster.transform.parent = transform;
        }

        public void RemoveMonster(Monster monster)
        {
            if (monsters.Count < 1 || !monsters.Contains(monster)) return;

            monsters.Remove(monster);
        }

    }
}