﻿
using System;
using System.IO;

namespace MonsterDuos
{
    public class GameDataReader
    {
        BinaryReader reader;

        public GameDataReader(BinaryReader reader)
        {
            this.reader = reader;
        }

        public float ReadFloat()
        {
            return reader.ReadSingle();
        }

        public int ReadInt()
        {
            return reader.ReadInt32();
        }

        internal string ReadString()
        {
            return reader.ReadString();
        }

        public MonsterStats ReadStats()
        {
            int Level = ReadInt();
            int HP = ReadInt();
            int HPCurrent = ReadInt();
            int Attack = ReadInt();
            int Defense = ReadInt();
            int Speed = ReadInt();
            int Experience = ReadInt();
            MonsterStats stats = new MonsterStats(Level, HP, Attack, Defense, Speed, Experience);
            stats.HPCurrent = HPCurrent;
            return stats;
        }
    }
}