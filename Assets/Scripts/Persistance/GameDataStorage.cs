﻿using System.IO;
using UnityEngine;

namespace MonsterDuos
{
    public class GameDataStorage : MonoBehaviour
    {
        string savePath;

        void Awake()
        {
            savePath = Path.Combine(Application.persistentDataPath, "saveFile");
        }

        public void Save(IPersistable persistable)
        {
            using (var writer = GetWriter())
            {
                persistable.Save(new GameDataWriter(writer));
            }
        }

        public void Load(IPersistable persistable)
        {
            using (var reader = GetReader())
            {
                persistable.Load(new GameDataReader(reader));
            }
        }

        BinaryReader GetReader()
        {
            return new BinaryReader(File.Open(savePath, FileMode.Open));
        }

        BinaryWriter GetWriter()
        {
            return new BinaryWriter(File.Open(savePath, FileMode.Create));
        }
    }
}