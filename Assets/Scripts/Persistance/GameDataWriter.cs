﻿
using System.IO;

namespace MonsterDuos
{
    public class GameDataWriter
    {

        BinaryWriter writer;


        public GameDataWriter(BinaryWriter writer)
        {
            this.writer = writer;
        }

        public void Write(float value)
        {
            writer.Write(value);
        }

        public void Write(int value)
        {
            writer.Write(value);
        }

        public void Write(string value)
        {
            writer.Write(value);
        }

        public void Write(MonsterStats stats)
        {
            Write(stats.Level);
            Write(stats.HP);
            Write(stats.HPCurrent);
            Write(stats.Attack);
            Write(stats.Defense);
            Write(stats.Speed);
            Write(stats.Experience);
        }
    }
}