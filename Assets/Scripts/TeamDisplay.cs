﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MonsterDuos {
	public class TeamDisplay : MonoBehaviour {

        [SerializeField] Team team;
        [SerializeField] GameObject listEntryPrefab;
        [SerializeField] RectTransform entryContainer;
        [SerializeField] GameDataStorage storage;

        void RemoveChildren()
        {
            for (int i = entryContainer.childCount - 1; i >= 0; i--)
            {
                Destroy(entryContainer.GetChild(i).gameObject);
            }
        }

        public void UpdateDisplay()
        {
            RemoveChildren();
            foreach (var monster in team.Monsters)
            {
                ButtonListEntry entry = Instantiate(listEntryPrefab, entryContainer).GetComponent<ButtonListEntry>();
                Monster m = monster;
                entry.Text = monster.Name;
                entry.Button.onClick.AddListener(() => Remove(entry.gameObject, m));
            }
        }

        public void Remove(GameObject removeGameObject, Monster monster)
        {
            Destroy(removeGameObject);
            team.RemoveMonster(monster);
            Destroy(monster.gameObject);
        }

        public void Load()
        {
            storage.Load(team);
            UpdateDisplay();
        }

        public void Save()
        {
            storage.Save(team);
        }
	}
}