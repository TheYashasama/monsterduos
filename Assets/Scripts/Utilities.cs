﻿using UnityEngine;

namespace MonsterDuos
{
    public struct MonsterStats
    {
        public int Level;
        public int HP;
        public int Attack;
        public int Defense;
        public int Speed;
        public int Experience;

        int hpcurrent;
        public int HPCurrent
        {
            get { return hpcurrent; }
            set { hpcurrent = Mathf.Clamp(value, 0, HP); }
        }

        public MonsterStats(int level, int hp, int attack, int defense, int speed, int experience)
        {
            Level = level;
            HP = hp;
            hpcurrent = hp;
            Attack = attack;
            Defense = defense;
            Speed = speed;
            Experience = experience;
        }
    }

    public class Utilities
    {
        public static int MaximumMonsterlevel = 50;

        public static int MaximumMoves = 4;

        public static float DamageDampener = 0.3f;

        public static float DeathTimer = 1f;

        public static float WeaknessModifier = 1.5f;

        public static float ResitanceModifer = 0.5f;

        public static float ElementBonus = 1.5f;

        public static int TeamSize = 4;

        public static int CalculateDamage(Monster usingMonster, Monster target, MoveData move)
        {
            float attackDefenseRatio = (float)usingMonster.Attack / target.Defense;
            float typeModifier = move.GetModifier(target);
            float elementBonus = move.GetBonus(usingMonster);
            int amount = (int)(move.Power * attackDefenseRatio * typeModifier * elementBonus * DamageDampener);
            return amount;
        }
    }
}